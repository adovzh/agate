package dan.agate.profile;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.junit.Test;

import java.util.Collection;
import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * @author Alexander Dovzhikov
 */
public class ProfilerTest {
	private Mockery context = new JUnit4Mockery();

	@Test
	public void rootNode() {
		final TimeProvider provider = context.mock(TimeProvider.class);

		context.checking(new Expectations() {{
			exactly(2).of(provider).currentTime();
			will(onConsecutiveCalls(returnValue(1000L), returnValue(2000L)));
		}});

		String rootNode = "cuebic";
		Profiler profiler = new Profiler(rootNode, provider);
		ProfilerNode node = profiler.getReport();

		assertNotNull(node);
		assertEquals(rootNode, node.getName());
		assertEquals(rootNode, node.getFQName());
		assertTrue(node.isStopped());
		assertEquals(1000L, node.getElapsedTime());
		assertEquals(1000L, node.getStartTime());
		assertEquals(2000L, node.getEndTime());

		Collection<ProfilerNode> children = node.getChildren();
		assertTrue(children.isEmpty());
	}

	@Test
	public void singleChild() {
		final TimeProvider provider = context.mock(TimeProvider.class);

		context.checking(new Expectations() {{
			exactly(3).of(provider).currentTime();
			will(onConsecutiveCalls(returnValue(1000L), returnValue(2000L), returnValue(3000L)));
		}});

		String rootNode = "cuebic";
		String childNode = "child";
		Profiler profiler = new Profiler(rootNode, provider);
		profiler.start(childNode);
		ProfilerNode node = profiler.getReport();

		assertNotNull(node);
		assertEquals(rootNode, node.getName());
		assertEquals(rootNode, node.getFQName());
		assertTrue(node.isStopped());
		assertEquals(2000L, node.getElapsedTime());
		assertEquals(1000L, node.getStartTime());
		assertEquals(3000L, node.getEndTime());

		Collection<ProfilerNode> children = node.getChildren();
		assertEquals(1, children.size());
		ProfilerNode child = children.iterator().next();

		assertNotNull(child);
		assertEquals(childNode, child.getName());
		assertEquals(rootNode + "." + childNode, child.getFQName());
		assertTrue(child.isStopped());
		assertEquals(1000L, child.getElapsedTime());
		assertEquals(2000L, child.getStartTime());
		assertEquals(3000L, child.getEndTime());
	}

	@Test
	public void siblings() {
		final TimeProvider provider = context.mock(TimeProvider.class);

		context.checking(new Expectations() {{
			exactly(4).of(provider).currentTime();
			will(onConsecutiveCalls(returnValue(1000L), returnValue(1500L), returnValue(3000L), returnValue(3200L)));
		}});

		String rootNode = "cuebic";
		String childNode = "child";
		String brotherNode = "brother";
		Profiler profiler = new Profiler(rootNode, provider);
		profiler.start(childNode);
		profiler.next(brotherNode);
		ProfilerNode node = profiler.getReport();

		assertNotNull(node);
		assertEquals(rootNode, node.getName());
		assertEquals(rootNode, node.getFQName());
		assertTrue(node.isStopped());
		assertEquals(2200L, node.getElapsedTime());
		assertEquals(1000L, node.getStartTime());
		assertEquals(3200L, node.getEndTime());

		Collection<ProfilerNode> children = node.getChildren();
		assertEquals(2, children.size());
		Iterator<ProfilerNode> iterator = children.iterator();
		ProfilerNode child = iterator.next();

		assertNotNull(child);
		assertEquals(childNode, child.getName());
		assertEquals(rootNode + "." + childNode, child.getFQName());
		assertTrue(child.isStopped());
		assertEquals(1500L, child.getElapsedTime());
		assertEquals(1500L, child.getStartTime());
		assertEquals(3000L, child.getEndTime());

		child = iterator.next();
		assertNotNull(child);
		assertEquals(brotherNode, child.getName());
		assertEquals(rootNode + "." + brotherNode, child.getFQName());
		assertTrue(child.isStopped());
		assertEquals(200L, child.getElapsedTime());
		assertEquals(3000L, child.getStartTime());
		assertEquals(3200L, child.getEndTime());
	}
}
