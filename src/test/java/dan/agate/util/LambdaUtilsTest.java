/*
 * Copyright (c) 2012 Alexander Dovzhikov <alexander.dovzhikov@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY ALEXANDER DOVZHIKOV ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL dan OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * LambdaUtilsTest.java
 *
 * Created on 08.07.2012 23:57:22
 */

package dan.agate.util;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Random;

import static org.junit.Assert.*;

/**
 * @author Alexander Dovzhikov
 */
public class LambdaUtilsTest {
    @Test
    public void map() {
        SingleArgFunc<Integer,Integer> doubler = new SingleArgFunc<Integer, Integer>() {
            @Override
            public Integer eval(Integer arg) {
                return arg * 234;
            }
        };
        assertNull(LambdaUtils.map(null, doubler));

        Random r = new Random();
        Collection<Integer> input = new ArrayList<Integer>();

        for (int i = 0; i < 100; i++) {
            input.add(r.nextInt());
        }

        Collection<Integer> output = LambdaUtils.map(input, doubler);
        assertNotNull(output);
        assertEquals(input.size(), output.size());

        Iterator<Integer> inputIterator = input.iterator();
        Iterator<Integer> outputIterator = output.iterator();

        while (inputIterator.hasNext()) {
            assertEquals(doubler.eval(inputIterator.next()), outputIterator.next());
        }
    }
}
