package dan.agate.util;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertEquals;

/**
 * @author Alexander Dovzhikov
 */
public class IOUtilsTest {
	private static final String INPUT = "This is just a test\nof multiline character stream\nto be read as Java String\n";

	@Test
	public void readStreamAsUTF8String() throws IOException {
		InputStream is = new ByteArrayInputStream(INPUT.getBytes(StandardCharsets.UTF_8));
		String result = IOUtils.readStreamAsUTF8String(is);
		assertEquals(INPUT, result);
	}

	@Test
	public void readLargeStreamAsUTF8String() throws IOException {
		int numInputs = 100;
		StringBuilder largeStringBuilder = new StringBuilder(INPUT.length() * numInputs);

		for (int i = 0; i < numInputs; i++)
			largeStringBuilder.append(INPUT);

		String largeInput = largeStringBuilder.toString();
		InputStream is = new ByteArrayInputStream(largeInput.getBytes(StandardCharsets.UTF_8));
		String result = IOUtils.readStreamAsUTF8String(is);
		assertEquals(largeInput, result);
	}
}
