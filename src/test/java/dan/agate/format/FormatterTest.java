/*
 * Copyright (c) 2012 Alexander Dovzhikov <alexander.dovzhikov@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY ALEXANDER DOVZHIKOV ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL dan OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * FormatterTest.java
 *
 * Created on 31.03.2012 18:03:51
 */

package dan.agate.format;

import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

/**
 * @author Alexander Dovzhikov
 */
public class FormatterTest {
    @Test
    public void simple() {
        Formatter f = new Formatter();
        Assert.assertEquals("Identity", f.format("Identity"));
        
        long currentTime = System.currentTimeMillis();
        String ctText = new Date(currentTime).toString();
        Assert.assertEquals("2 + 2 = $4 (" + ctText + ")", f.format("{} + {#} = {} ({})", 2, 2L, "$4", currentTime));
        Assert.assertEquals(ctText, f.format("{}", currentTime));
        Assert.assertEquals(String.valueOf(currentTime), f.format("{#}", currentTime));
    }
}
