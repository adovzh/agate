/*
 * Copyright (c) 2012 Alexander Dovzhikov <alexander.dovzhikov@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY ALEXANDER DOVZHIKOV ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL dan OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * MaybeTest.java
 *
 * Created on 31.03.2012 23:33:54
 */

package dan.agate.monad;

import dan.agate.monad.pojo.Address;
import dan.agate.monad.pojo.Company;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Alexander Dovzhikov
 */
public class MaybeTest {
    private final Func<Company,Address> mAddress = new Func<Company, Address>() {
        public Address eval(Company company) {
            return company.getAddress();
        }
    };

    private final Func<Company,String> mName = new Func<Company, String>() {
        public String eval(Company company) {
            return company.getName();
        }
    };

    private final Func<Address,String> mStreet = new Func<Address, String>() {
        public String eval(Address address) {
            return address.getStreet();
        }
    };

    @Test
    public void someTest() {
        Company company = new Company(new Address("Sesame"), "Comp1");
        Assert.assertEquals("Sesame", Options.chainMaybe(company, mAddress, mStreet));
        Assert.assertEquals("Comp1", Options.chainMaybe(company, mName));
    }

    @Test
    public void someNone() {
        Company company = new Company(null, "Comp2");
        Assert.assertNull(Options.chainMaybe(company, mAddress, mStreet));
        Assert.assertEquals("Comp2", Options.chainMaybe(company, mName));
        Assert.assertNull(Options.chainMaybe(null, mName));
    }
}
