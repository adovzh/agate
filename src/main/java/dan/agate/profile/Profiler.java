package dan.agate.profile;

import org.apache.commons.lang.StringEscapeUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Alexander Dovzhikov
 */
public class Profiler {
	private static final ThreadLocal<SimpleDateFormat> SDF = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		}
	};

	private static SimpleDateFormat getDateFormat() {
		return SDF.get();
	}

	private ProfilerNode current;
	private final TimeProvider provider;

	public Profiler(String rootName) {
		this(rootName, TimeProvider.SYSTEM);
	}

	public Profiler(String rootNode, TimeProvider provider) {
		this.provider = provider;
		start(rootNode);
	}

	public void start(String nodeName) {
		start(nodeName, getCurrentTime());
	}

	private long getCurrentTime() {
		return provider.currentTime();
	}

	public void next(String nodeName) {
		ProfilerNode current = this.current;
		stop();
		start(nodeName, current.getEndTime());
	}

	public void stop() {
		stop(getCurrentTime());
	}

	private void start(String nodeName, long start) {
		current = new ProfilerNode(current, nodeName, start);
	}

	private void stop(long end) {
		current = current.stop(end);
	}

	public ProfilerNode getReport() {
		ProfilerNode root;
		long endTime = getCurrentTime();

		do {
			root = current;
			stop(endTime);
		} while (current != null);

		current = root;

		return root;
	}

	public String getXMLReport() throws ParserConfigurationException, TransformerException {
		return new XMLBuilder().build();
	}

	private class XMLBuilder {
		Document document;

		XMLBuilder() throws ParserConfigurationException {
			document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
		}

		String build() throws TransformerException {
			ProfilerNode root = getReport();
			processElement(root, document);
			return transform();
		}

		void processElement(ProfilerNode profilerNode, Node node) {
			SimpleDateFormat sdf = getDateFormat();
			String tagName = StringEscapeUtils.escapeXml(profilerNode.getName());
			Element nodeElement = document.createElement(tagName);
			nodeElement.setAttribute("start", sdf.format(new Date(profilerNode.getStartTime())));
			nodeElement.setAttribute("end", sdf.format(new Date(profilerNode.getEndTime())));
			nodeElement.setAttribute("elapsed", String.format("%dms", profilerNode.getElapsedTime()));

			for (ProfilerNode childNode : profilerNode.getChildren()) {
				processElement(childNode, nodeElement);
			}

			node.appendChild(nodeElement);
		}

		String transform() throws TransformerException {
			StringWriter writer = new StringWriter();
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			transformer.transform(new DOMSource(document), new StreamResult(writer));
			return writer.toString();
		}
	}
}
