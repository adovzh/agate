package dan.agate.profile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Collections;
import java.util.Deque;

/**
 * @author Alexander Dovzhikov
 */
public class ProfilerNode {
	private static final Logger log = LoggerFactory.getLogger(ProfilerNode.class);

	private final ProfilerNode parent;
	private final Deque<ProfilerNode> children = new ArrayDeque<>();
	private final String name;
	private final long start;
	private long end;
	private boolean stopped = false;

	public ProfilerNode(ProfilerNode parent, String name, long startTime) {
		this.parent = parent;
		this.name = name;
		this.start = startTime;

		if (parent != null)
			parent.children.add(this);
	}

	public ProfilerNode stop(long endTime) {
		if (stopped) {
			log.warn("Trying to stop stopped profiler node: {}", getFQName());
		} else {
			end = endTime;
			stopped = true;
		}

		return parent;
	}

	public ProfilerNode getParent() {
		return parent;
	}

	public String getName() {
		return name;
	}

	public String getFQName() {
		StringBuilder sb = new StringBuilder();
		buildFQName(sb);
		return sb.toString();
	}

	private void buildFQName(StringBuilder sb) {
		if (parent != null) {
			parent.buildFQName(sb);
			sb.append('.');
		}
		sb.append(getName());
	}

	public long getElapsedNano() {
		return end - start;
	}

	public long getStartTime() {
		return start;
	}

	public long getEndTime() {
		return end;
	}

	public long getElapsedTime() {
		return end - start;
	}

	public boolean isStopped() {
		return stopped;
	}

	public Collection<ProfilerNode> getChildren() {
		return Collections.unmodifiableCollection(children);
	}
}
