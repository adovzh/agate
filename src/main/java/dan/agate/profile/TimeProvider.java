package dan.agate.profile;

/**
 * @author Alexander Dovzhikov
 */
public interface TimeProvider {

	public final TimeProvider SYSTEM = new TimeProvider() {
		@Override
		public long currentTime() {
			return System.currentTimeMillis();
		}
	};

	long currentTime();
}
