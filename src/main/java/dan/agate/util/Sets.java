package dan.agate.util;

import java.util.*;

/**
 * @author Alexander Dovzhikov
 */
public class Sets {
    private Sets() {
    }

    public static <T> Set<List<T>> product(List<Set<T>> sets) {
        if (sets.isEmpty())
            return Collections.emptySet();

        return product(sets.iterator());
    }

    /*
        Ugly implementation with copying all around
        also, not tested.
     */
    private static <T> Set<List<T>> product(Iterator<Set<T>> sets) {
        if (!sets.hasNext())
            return Collections.singleton(Collections.<T>emptyList());

        Set<T> nextSet = sets.next();
        Set<List<T>> remainder = product(sets);
        Set<List<T>> result = new LinkedHashSet<>();

        for (T item : nextSet) {
            if (item == null)
                continue;

            for (List<T> ts : remainder) {
                List<T> resultList = new ArrayList<>();
                resultList.add(item);
                resultList.addAll(ts);
                result.add(resultList);
            }
        }

        return result;
    }

    public static void main(String[] args) {
        Set<String> ab = new LinkedHashSet<>();
        Set<String> cd = new LinkedHashSet<>();

        ab.add("a"); ab.add("b"); ab.add("x");
        cd.add("c"); cd.add("d");

        List<Set<String>> sets = new ArrayList<>();
        sets.add(ab); sets.add(cd);

        Set<List<String>> prd = product(sets);

        System.out.println(prd);
    }
}
