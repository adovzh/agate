package dan.agate.util;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.nio.charset.StandardCharsets;

/**
 * @author Alexander Dovzhikov
 */
public class IOUtils {
	private static final int BUF_SIZE = 4096;

	private IOUtils() {
	}

	public static String readStreamAsUTF8String(InputStream is) throws IOException {
		return readStreamAsString(is, StandardCharsets.UTF_8);
	}

	public static String readStreamAsString(InputStream is, Charset charset) throws IOException {
		ReadableByteChannel channel = Channels.newChannel(is); // channel to read from
		CharsetDecoder decoder = getDefaultDecoder(charset);
		ByteBuffer bytes = ByteBuffer.allocateDirect(BUF_SIZE); // buffer for bytes
		CharBuffer chars = CharBuffer.allocate(BUF_SIZE); // buffer for decoded characters
		char[] array = chars.array();
		StringBuilder sb = new StringBuilder();

		while (channel.read(bytes) != -1) {
			bytes.flip(); // read mode
			decoder.decode(bytes, chars, false);

			chars.flip(); // read mode
			sb.append(array, chars.position(), chars.remaining());

			bytes.compact(); // write mode
			chars.clear(); // clear and write mode
		}

		// decode the last portion
		bytes.flip();
		decoder.decode(bytes, chars, true);
		decoder.flush(chars);

		// write to string buffer
		chars.flip();
		sb.append(array, chars.position(), chars.remaining());

		return sb.toString();
	}

	private static CharsetDecoder getDefaultDecoder(Charset charset) {
		CharsetDecoder decoder = charset.newDecoder();
		decoder.onMalformedInput(CodingErrorAction.IGNORE);
		decoder.onUnmappableCharacter(CodingErrorAction.IGNORE);
		return decoder;
	}
}
