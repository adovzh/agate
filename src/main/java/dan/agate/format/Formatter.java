/*
 * Copyright (c) 2012 Alexander Dovzhikov <alexander.dovzhikov@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY ALEXANDER DOVZHIKOV ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL dan OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Formatter.java
 *
 * Created on 31.03.2012 17:59:52
 */

package dan.agate.format;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Substitutes {} with arg.toString() except Long arguments
 * that will be built with new Date(arg).toString(). To fallback
 * default behaviour for Long use {#} pattern.
 *
 * @author Alexander Dovzhikov
 */
public class Formatter {
    private static final Pattern DELIM = Pattern.compile("\\{(#?)\\}");

   	public Formatter() {
   	}

   	public String format(String format, Object ... args) {
   		StringBuilder sb = new StringBuilder();
   		List<String> chunks = new ArrayList<>();
   		Matcher m = DELIM.matcher(format);
   		Object arg;
   		int index = 0;
   		int argIndex = 0;

   		while (m.find()) {
   			chunks.add(format.substring(index, m.start()));
   			arg = argIndex < args.length ? args[argIndex++] : format.substring(m.start(), m.end() + 1);
   			if (arg instanceof Long && !"#".equals(m.group(1)))
   				arg = new Date((Long)arg);
   			chunks.add(arg.toString());
   			index = m.end();
   		}

   		chunks.add(format.substring(index, format.length()));

   		for (String chunk : chunks)
   			sb.append(chunk);

   		return sb.toString();
   	}
}
